---
title: "Quantitative Methods for HIV Researchers"
subtitle: "HIV Alignments Database"
author: "Janice M. McCarthy"
date: "August 27, 2019"
output: html_document
---

This exercise is based on the [HIV sequence database](https://www.hiv.lanl.gov/content/sequence/HIV) at Los Alamos National Laboratory. It is a repository of aligned HIV sequences from various experiments. Begin by downloading a zipped archive of 'Special Interest Data set 10'. This is a longitudinal data set of viral epitopes and T-cell immunity measures. There are Excel spreadsheets corresponding to clinical data (CD4 and CD8 counts plus viral load), counts of epitopes that are strongly selected, weakly selected, etc.

There are also fasta files, presumably used to get the alignments and counts, but we won't be using those.

Execute the setup chunk:

```{r setup, include=FALSE, root.dir = "~/hiv-workshop-materials2019/Code/Day6_ParticpantsChoice"}
library(purrr)
library(tidyverse)
library(readxl)
```

First, use the following code block to download the zip file and create a list of dataframes with the contents of each Excel spreadsheet. I copied this code from a help forum.

```{r}

url <- "url-to-your-zip"
path_zip <- "your-downloaded-zip-local-path"
path_unzip <- "path-where-to-save-unzip-files"
destfile <- "archive.zip"

# download zip
curl::curl_download(url, destfile = paste(path_zip, destfile, sep = "/"))

#unzip
unzip(destfile, exdir = path)

# list all files
files <- list.files(path = path_unzip)
 
# apply map_df() to iterate read_csv over files
data <- map_df(paste(path_unzip, files, sep = "/"),
                     read_csv,
                     ## additional params to read_csv here
                          )

```

```{r setup, include=FALSE, root.dir = "~/hiv-workshop-materials2019/Code/Day6_ParticpantsChoice"}
library(purrr)

#First, the url is obtained by going to the download page, and right clicking on the `Download zip` link, saving the link in the clipboard.

url <- "https://www.hiv.lanl.gov/content/sequence/HIV/SI_alignments/data/set10_PIC1362_Liu.zip"

#Next is the target path to place the zip and then where to unzip it.

path_zip <- "~/hiv-workshop-materials2019"
path_unzip <- "~/hiv-workshop-materials2019/HIVAlignmentDatabaseExample"
destination_file <- "set10_PIC1362_Liu.zip"

# download zip
curl::curl_download(url, destfile = paste(path_zip, destination_file, sep = "/"))

#unzip
unzip(zipfile = paste(path_zip, destination_file, sep = "/"), exdir = path_unzip)

# list all files - this gives us a list of files in the directory we have just unzipped
files <- list.files(path = path_unzip)
 
# apply map_df() to iterate read_csv over files
df_list <- map_df(paste(path_unzip, files, sep = "/"), read_csv)

```

Oops! This code does not work out-of-the-box. One problem is that we don't have csv files. We have xlsx files.

```{r}
df_list <- map_df(paste(path_unzip, files, sep = "/"), read_excel)

```

We still have a problem. What types of files are in the variable 'files'?

```{r}
files
```
The read_excel function has no idea what to do with many of these. Lets get rid of the non-excel files. Create a new file list with only the excel filenames from `files`, then try to read again.

```{r}
files %>% as_tibble() %>% filter(str_detect(value, ".xls")) %>%  as_vector() -> files

df_list <- map_df(paste(path_unzip, files, sep = "/"), read_excel)

```

Yikes! What happened here? Looks like we put *all* the data files into *one* dataframe. That makes sense - because we used the `purrr` function `map_df`. Remember that the `map_` functions in `purrr` return the type of object that comes after the underscore. We want a *list*. So, let's try `map` (which returns the very general type of `list`).

```{r}
df_list <- map(paste(path_unzip, files, sep = "/"), read_excel)

df_list

```

Ok. Now we have a list, with each file in a different dataframe. But there seem to be errors. (Red text usually - not always, but usually means there is a problem.) Let's go over to the website and look at the spreadsheets. (We should have done this first. This has been a painful exercise in 'know how your input is formatted')

We want the `Longitudinal CTL Responses` data.

[HIV sequence database](https://www.hiv.lanl.gov/content/sequence/HIV/SI_alignments)

Ok. We're back. And we just found out the headers are a bit of a problem. The clinical data is fine, but the persistent_epitopes file has multiple headers. Now you know why statisticians hate excel. It's pretty and all, but getting it read in correctly can be a real pain, depending on formatting.

There seem to be 3 different types of formatting: one has a main header that just informs us that these are CTL responses, followed by two header lines that describe the epitope; a second has the main header, then the tag 'original' or 'variant' followed by a 2 line header that defines the epitope sequence in the first line, and then the description in the 'original' column, and is blank for variants (this is actually helpful, because we can figure out which rows are variants). The third is the one we like. It has one header row that describes the column entries.

We are going to need to do this manually. We'll leave the simple one (clinical data) alone for now. Let's figure out how to handle the 'strongly selected' epitope file.

We'll need to do this stepwise, extracting parts of the file at a time, then re-constructing into a nice, tidy tibble.

```{r}
# extract second  and third rows into a tibble.

colnames_strong <- read_excel("~/hiv-workshop-materials2019/HIVAlignmentDatabaseExample/strongly_selected_epitopes.xlsx", 
                              skip = 1, n_max = 2)

colnames_strong
```

Well, we have a small problem, and that is that the first column has a header that spans two cells (yuck!). We can tell read_excel to ignore this using the range argument.

```{r}
colnames_strong <- read_excel("~/hiv-workshop-materials2019/HIVAlignmentDatabaseExample/strongly_selected_epitopes.xlsx", skip = 1, n_max = 2, range = cell_cols("B:I"))

colnames_strong
```

Ack! Not what we wanted. The ranges option has overridden our 'n_max' option. But we can extract what we want from this - the first two rows contain the column names.

```{r}
colnames_strong <- read_excel("~/hiv-workshop-materials2019/HIVAlignmentDatabaseExample/strongly_selected_epitopes.xlsx",
                              skip = 1, n_max = 2, range = cell_cols("B:I"))

colnames_strong <- colnames_strong[1:2, ]

colnames_strong
```

Now, lets get a character vector that has both strings for each epitope.

```{r}

colnames_strong <- str_c(colnames_strong[1,], colnames_strong[2,])
```

Now we are ready to extract the data into a tibble. We'll just skip the first 3 rows.

```{r}
strongly_selected_df <- read_excel("~/hiv-workshop-materials2019/HIVAlignmentDatabaseExample/strongly_selected_epitopes.xlsx", 
                                   skip = 3, col_names = c("DPS", colnames_strong))

strongly_selected_df

```

Almost there. We notice that the read function decided that DPS is a character variable. That's probably not what we want. We can fix this in one or two ways: We can just use `as_integer` as we have done before. We can also tell the read function that we want DPS to be an integer value by setting the `col_types` option.

```{r}
strongly_selected_df <- read_excel("~/hiv-workshop-materials2019/HIVAlignmentDatabaseExample/strongly_selected_epitopes.xlsx", 
                                   skip = 3, col_names = c("DPS", colnames_strong), col_types = c("numeric", rep("guess", length(colnames_strong))))

strongly_selected_df
```
One last thing: We noticed that the last two rows of the .xlsx file are not data. They are one blank row and one row that notes DPS stands for 'days post onset of acute symptoms'. Might as well remove these last two rows:

```{r}
strongly_selected_df %>% filter(!is.na(DPS)) -> strongly_selected_df
```


Whew! Well, that one is done.

Exercise: Fix the other files that have the same format.

Do the same for the files that have the same structure.

Ans: First weakly selected epitopes.

```{r}

### Note that this file has columns B through K.

colnames_weak <- read_excel("~/hiv-workshop-materials2019/HIVAlignmentDatabaseExample/weakly_selected_epitopes.xlsx", 
                            skip = 1, n_max = 2, range = cell_cols("B:K"))

colnames_weak

colnames_weak <- str_c(colnames_weak[1,], colnames_weak[2,])

colnames_weak
```

```{r}
weakly_selected_df <- read_excel("~/hiv-workshop-materials2019/HIVAlignmentDatabaseExample/weakly_selected_epitopes.xlsx", 
                                 skip = 3, col_names = c("DPS", colnames_weak), col_types = c("numeric", rep("guess", length(colnames_weak))))

weakly_selected_df

weakly_selected_df %>% filter(!is.na(DPS)) -> weakly_selected_df
```
 
Persistent - in one cell

```{r}

### Note that this file has columns B through K.

colnames_persistent <- read_excel("~/hiv-workshop-materials2019/HIVAlignmentDatabaseExample/persistent_epitopes.xlsx", 
                                  skip = 1, n_max = 2, range = cell_cols("B:H"))

colnames_persistent

colnames_persistent <- str_c(colnames_persistent[1,], colnames_persistent[2,])

colnames_persistent

persistent_df <- read_excel("~/hiv-workshop-materials2019/HIVAlignmentDatabaseExample/persistent_epitopes.xlsx", 
                            skip = 3, col_names = c("DPS", colnames_persistent), col_types = c("numeric", rep("guess", length(colnames_persistent))))

persistent_df

persistent_df %>% filter(!is.na(DPS)) -> persistent_df

persistent_df
```
Now, onto the next file type that has an added layer of complexity: We want to keep the 'original' and 'variant' tags somehow. 

It's helpful that theooriginal' has the epitope decription on the second (true) header line, so we'll use that to create a 'variant/original' tag for each column.

```{r}
colnames_vars_env <- read_excel("~/hiv-workshop-materials2019/HIVAlignmentDatabaseExample/variant_env_epitopes.xlsx", 
                                skip = 3, n_max = 2, range = cell_cols("B:AF"))

colnames_vars_env

var_orig <- ifelse(is.na(colnames_vars_env[3,]),"Variant", "Original") 

```
Now we'll concatenate the 2 rows for 'original'

```{r}

colnames_vars_env <- ifelse(!is.na(colnames_vars_env[3,]),str_c(colnames_vars_env[2,], colnames_vars_env[3,]),colnames_vars_env[2,]) 

colnames_vars_env
```
and concatenate the two header lines we've just created.

```{r}

colnames_vars_env <- str_c(var_orig, colnames_vars_env, sep = "-")

colnames_vars_env

```
Now, put it all together:
```{r}
vars_env_df <- read_excel("~/hiv-workshop-materials2019/HIVAlignmentDatabaseExample/variant_env_epitopes.xlsx", 
                          skip = 4, col_names = c("DPS", colnames_vars_env), col_types = c("numeric", rep("guess", length(colnames_vars_env))))

vars_env_df

vars_env_df %>% filter(!is.na(DPS)) -> vars_env_df

vars_env_df
```

Ok. That should do it. Now your turn:

Exercise:

Do the same for the other files in this format.


```{r}
read_variant_data_sets <- function(filename, range = "B:AF"){
  
                 colnames_vars <- read_excel(paste(path_unzip,filename, sep = "/"),
                                                 skip = 3, range = cell_cols(range))
                 
                 ############# find out how many columns
                 vars_df <- read_excel(paste(path_unzip,filename, sep = "/"), 
                                            skip = 4)
                 num_cols <- dim(vars_df)[2]
                 
                 print(num_cols)
                 print(filename)
                 
                 ######################
                  colnames_vars <- colnames_vars[,1:num_cols-1]
                  print(colnames_vars)

                  var_orig <- ifelse(is.na(colnames_vars[3,]),"Variant", "Original") 
                  colnames_vars <- str_c(var_orig, colnames_vars[2,], sep = "-")

                  print(colnames_vars)
                  vars_df <- read_excel(paste(path_unzip,filename, sep = "/"), 
                                            skip = 4, col_names = c("DPS", colnames_vars), 
                                            col_types = c("numeric", rep("guess", length(colnames_vars))))

                  vars_df

                  vars_df %>% filter(!is.na(DPS)) -> vars_df

                  vars_df

}

files %>% as_tibble() %>% filter(str_detect(value, "variant")) %>% as_vector -> var_files

#as_tibble(filename = var_files)

df_list <- map(var_files, read_variant_data_sets)
```


