---
title: "Quantitative Methods for HIV Researchers"
subtitle: "Part II Day 4 Exercises"
author: "Tina Davenport"
date: "November 1, 2019"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
Sys.setenv("LANGUAGE"="En")
Sys.setlocale("LC_ALL", "English")


#############################################################################
##                                                                         ##
##  Author: tina.davenport@duke.edu                                        ##
##  Program: Day3_Exercises_solutions                                      ##
##  Date: 11/01/2019                                                       ##
##  Purpose: This program performs some of the tests outined in the notes  ##
##           and gives solutions to the in class exercises and examples    ##
##                                                                         ##
##                                                                         ##
##  Change Log:                                                            ##
##  11/01/2019 File created                                                ##
##                                                                         ##
#############################################################################

setwd("~/hiv-workshop-materials2019/PartII/Day4/")
library(tidyverse)
```


# In-Class Examples and Exercises

### Slide 8

```{r}
ACTG <- read.csv(file="ACTG_r25.csv", header=TRUE, na.strings="")
  # tmp <- subset(ACTG, week==0);  summary(tmp)

# Research question: among white males in, is treatment A effective at
# improving HIV outcomes from baseline to week 4?

# Note: couldn't find "pivot_wider" function in tidyr or tidyverse, so
# converting back to base R code
ACTGa <- subset(ACTG, (week %in% c(0,4) & Arm=="A" & sex=="M" & race=="White"),
    select=c(ntisid, week, hivRNA, CD4, CD8))  # 32 white males in arm A

# reshape from long to wide - note: don't need to do this for t.test function!
ACTGa_wide <- reshape(data=ACTGa, v.names=c("hivRNA", "CD4", "CD8"),
    timevar="week", idvar="ntisid", direction="wide", sep="w")
ACTGa_wide$HIVd <- ACTGa_wide$hivRNAw4-ACTGa_wide$hivRNAw0
ACTGa_wide$CD4d <- ACTGa_wide$CD4w4-ACTGa_wide$CD4w0
ACTGa_wide$CD8d <- ACTGa_wide$CD8w4-ACTGa_wide$CD8w0
summary(ACTGa_wide)


# just look at CD4 counts for the first example
dat <- na.omit(subset(ACTGa_wide, select=c(ntisid, CD4w0, CD4w4, CD4d)))


#------------------  Step 1: Know data, check assumptions  -----------------#
par(mfcol=c(2,3))
hist(dat$CD4w0, cex=1.3, cex.lab=1.3, cex.axis=1.3, cex.main=1.5,
    xlab="Pre-treatment CD4", freq=FALSE, main="Pre-treatment")
  lines(density(dat$CD4w0), col=4, lwd=3)
  curve(dnorm(x, mean=mean(dat$CD4w0), sd=sd(dat$CD4w0)), add=TRUE, col=2,
      lwd=3)
qqnorm(dat$CD4w0, lwd=2, cex.lab=1.3, cex.axis=1.3, main="")
  qqline(dat$CD4w0, col=2, lwd=3)

hist(dat$CD4w4, cex=1.3, cex.lab=1.3, cex.axis=1.3, cex.main=1.5,
    xlab="Post-treatment CD4", freq=FALSE, main="Post-treatment")
  lines(density(dat$CD4w4), col=4, lwd=3)
  curve(dnorm(x, mean=mean(dat$CD4w4), sd=sd(dat$CD4w4)), add=TRUE, col=2,
      lwd=3)
qqnorm(dat$CD4w4, lwd=2, cex.lab=1.3, cex.axis=1.3, main="")
  qqline(dat$CD4w4, col=2, lwd=3)

hist(dat$CD4d, cex=1.3, cex.lab=1.3, cex.axis=1.3, cex.main=1.5,
    xlab="Difference in CD4", freq=FALSE, main="Difference")
  lines(density(dat$CD4d), col=4, lwd=3)
  curve(dnorm(x, mean=mean(dat$CD4d), sd=sd(dat$CD4d)), add=TRUE, col=2, lwd=3)
qqnorm(dat$CD4d, lwd=2, cex.lab=1.3, cex.axis=1.3, main="")
  qqline(dat$CD4d, col=2, lwd=3)


#-------------------  Step 4: Test statistic and p-value  ------------------#  
round(c(summary(dat$CD4d), SD=sd(dat$CD4d), N=length(dat$CD4d)), 2)

TT <- mean(dat$CD4d)/(sd(dat$CD4d)/sqrt(length(dat$CD4d)))
TT

# p-value:
pt(q=TT, df=length(dat$CD4d)-1, lower.tail=TRUE) - 
    pt(q=-TT, df=length(dat$CD4d)-1, lower.tail=TRUE)

# CI
tcrit <- qt(p=0.975, df=length(dat$CD4d)-1, lower.tail=TRUE)
mean(dat$CD4d)+c(-tcrit*sd(dat$CD4d)/sqrt(length(dat$CD4d)),
    tcrit*sd(dat$CD4d)/sqrt(length(dat$CD4d)))
```


### Slide 12-17

```{r}
# using t.test, can do it in several ways
t.test(x=dat$CD4w4, y=dat$CD4w0, alternative="two.sided", mu=0, paired=TRUE)
t.test(CD4~week, data=ACTGa[-which(ACTGa$ntisid==76872558),], alternative="two.sided", mu=0, paired=TRUE)
t.test(x=dat$CD4d, alternative="two.sided", mu=0)
```


### Slide 29

```{r}
Yis <- dat$CD4d[dat$CD4d !=0]  # WSR Step 2: remove all the zeros from Yi star
RR <- rank(abs(Yis), na.last=NA, ties.method="average") # WSR Step 3: ranks
SS <- sign(Yis)                                         # WSR Step 3: signs
cbind(Yis, RR, SS)                                      # Slide 29
tapply(RR, SS, sum)                                     # WSR Step 4: Test statistics


wilcox.test(dat$CD4d, alternative="two.sided", mu=0, paired=FALSE, exact=TRUE, correct=FALSE) 
wilcox.test(dat$CD4d, alternative="two.sided", mu=0, paired=FALSE, exact=TRUE, correct=TRUE) 
```


### Slide 37

```{r}
YY <- data.frame(TRT=c(1,1,1,2,2,2,2), Yi=c(-8, -1, -5, 2, -4, 6, 1))
YY$rank <- rank(YY$Yi)  # Rank the data
YY

getW <- function(x){    # A function to compute W
    n <- length(x)
    sum(x)-n*(n+1)/2
}
tapply(YY$rank, YY$TRT, getW)

# p-value 
wilcox.test(Yi~TRT, data=YY, alternative="less", mu=0, exact=TRUE, correct=FALSE)
# Same since there are no ties
# wilcox.test(Yi~TRT, data=YY, alternative="less", mu=0, exact=TRUE, correct=TRUE)

```


### Slide 41
```{r}
Oij <- matrix(c(43,19,7,11), nrow=2)                # observed counts
addmargins(Oij)
Eij <- outer(rowSums(Oij),colSums(Oij)) / sum(Oij)  # Expected counts
  # 50*62/80; 50*18/80  
  # 30*62/80; 30*18/80  
addmargins(Eij)

XX <- sum((Oij-Eij)^2 / Eij)                        # Test Statistic
  # (43-38.75)^2/38.75 + (7-11.25)^2/11.25 +
  # (19-23.25)^2/23.25 + (11-6.75)^2/6.75

pchisq(q=XX, df=1, lower.tail=FALSE)  # p-value
chisq.test(x=Oij, correct=FALSE)      # using the function

#  or
HIV <- factor(c(rep("N", 50), rep("Y", 30)))
IVduse <- factor(c(rep("N", 43), rep("Y", 7), rep("N", 19), rep("Y", 11)))
# sample_n(data.frame(HIV, IVduse), 10)
chisq.test(x=HIV, y=IVduse, correct=FALSE)  
```


### Slide 60
```{r}
Oij <- matrix(c(230, 72, 192, 54, 63, 16, 15, 8), nrow=2)
Oij
# Eij <- outer(rowSums(Oij),colSums(Oij)) / sum(Oij)
# sum((Oij-Eij)^2 / Eij)
chisq.test(x=Oij, correct=FALSE)
```


### Slide 63-64
```{r}
Oij <- matrix(c(99,80,70, 190,100,30, 96,20,10), nrow=3)
Oij
# Eij <- outer(rowSums(Oij),colSums(Oij)) / sum(Oij)
# sum((Oij-Eij)^2 / Eij)
chisq.test(x=Oij, correct=FALSE)
```


### Slide 68-69
```{r}
Oij <- matrix(c(2, 5, 23, 30), nrow=2)
chisq.test(x=Oij, correct=FALSE)  # warning that some expected counts <5
outer(rowSums(Oij),colSums(Oij)) / sum(Oij)  # check Eij

fisher.test(Oij)  # use Fisher's test instead

```





<br>
<br>
<br>

## In-Depth Exercises
Below are exercises to reinforce concepts taught in the lecture part of the day.
<br>


### Question 1

Though the primary goal of the study was to assess the efficacy of three HIV medications on HIV outcomes (HIV RNA, CD4 counts, etc.) secondary outcomes included lipid measurements such as LDL and triglycerides. Among males in treatment A, did the study drug have an effect on change in triglycerides from baseline to week 24? Conduct a paired t-test.

a. First subset the ACTG data as needed
```{r}

```

b. Check assumptions
```{r}

```

c. Perform the test
```{r}

```

d. State the conclusions





### Question 2

Repeat Question 1 using a distribution free/nonparametric test.
```{r}

```



### Question 3
We found that treatment A was effective at increasing CD4 counts from baseline to week 4 among treatment naive white males with HIV. Is there a difference in treatment effect between treatment A and B on change in CD4 counts from baseline to week 4 in this sub population? 

a. First subset data as needed
```{r}

```


b. Check assumptions
```{r}

```


b. Based on the plots of the data, is a t-test reasonable or would you prefer a nonparametric test? Either way, conduct both.
```{r}

```



### Question 4

Is there a treatment difference between A and B on change in triglycerides from baseline to week 24 among males? Perform a parametric and nonparametric test.

a. First subset the ACTG data as needed
```{r}

```


b. Check assumptions
```{r}

```


c. Perform tests
```{r}

```


d. Draw conclusions





### Question 5
For the ACTG data, sampling was from one target population, thus, if we were to take a different sample, we would expect the counts for different characteristics (e.g., how many men and women there are in the trial) to change.

a. Thought question: Would chi-squared tests of characteristics of participants be one of homogeneity or independence?

b. At baseline, is there an association between race (Blacks and Whites only) and being sure that HIV medications have a positive effect (`howsure2`)?
```{r}

```


b. What about being "extremely sure" vs. all others and race? 
```{r}

```


c. Among men, is there an association between partner preference and race at baseline? 
```{r}

```


d. Which observed count(s) tend to deviate most from the expected counts?
```{r}

```




### Question 6

Researchers investigated the relationship between HIV seropositivity and STIs in a population at high risk for sexual acquisition of HIV. Subjects were 144 female commercial sex workers in Thailand of whom 62 were HIV-positive and 109 had an STI. In the HIV-negative group, 51 had a history of STD.

a. Estimate the risk difference, relative risk, odds, and odds ratio (and CIs) of HIV seropositivity.
```{r}

```


b. what are the conclusions from each measure?






