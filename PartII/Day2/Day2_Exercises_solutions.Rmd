---
title: "Quantitative Methods for HIV Researchers"
subtitle: "Part II Day 2 Exercises"
author: "Tina Davenport"
date: "October 18, 2019"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
Sys.setenv("LANGUAGE"="En")
Sys.setlocale("LC_ALL", "English")


setwd("~/hiv-workshop-materials2019/PartII/Day2/")
library(tidyverse)
```


# Brief In-Class Exercises

### Slide 46

The PMF example of babies born with type O blood is actually a binomial random variable $Y\sim$ Bin$(4,0.44)$. Redo the following events using the `binom` functions.

Let $A=\{$All four babies have type O blood$\}$. $P(A)=$?

Let $B=\{$At least two babies have type O blood$\}$. $P(B)=$?

Let $C=\{$Between 1 and 3 babies have type O blood$\}$. $P(C)=$?

$P(B \cup C)=$?

$P(\bar C)=$?

$P(A \cap C)=$?

$P(A \cap \bar C)=$?
```{r}
nn <- 4;  pi <- 0.44  # set binomial parameters
dbinom(x=4, size=nn, prob=pi)  # all 4 babies have O blood P(X=4)
1-pbinom(q=1, size=nn, prob=pi)  # At least 2 babies have O blood P(X>=2)
pbinom(q=3, size=nn, prob=pi) - pbinom(q=0, size=nn, prob=pi)  # 1-3 babies have O blood
1-pbinom(q=0, size=nn, prob=pi)  # P(B or c) = P(X>=1) = 1-P(X<1) = 1-P(X<=0)
dbinom(x=0, size=nn, prob=pi)+dbinom(x=4, size=nn, prob=pi)  # P(C') = P(X=0) + P(X=4)
```

<br>
Compute $E(Y)$, $E(Y^2)$, and var$(Y)$ using the `binom` functions.
```{r}
nn <- 4;  pi <- 0.44  # set binomial parameters
YY <- 0:4             # all possibly Y values

EY <- sum(YY*dbinom(x=YY, size=nn, prob=pi))  # E(Y)
EY
EY2 <- sum(YY^2*dbinom(x=YY, size=nn, prob=pi))  # E(Y^2)
EY2
sum((YY-EY)^2*dbinom(x=YY, size=nn, prob=pi))  # var(Y)
```


### Slide 55

The PDF example of survival is actually an exponential random variable  $Y\sim$ Exp$(\lambda=1/20)$. Redo the following events using the `pexp` function.


Let $A=\{$The person will survive less than 5 days$\}$. $P(A)=$?

Let $B=\{$The person will survive between 5 and 15 days$\}$. $P(B)=$?

Let $C=\{$The person will survive at least 15 days$\}$. $P(C)=$?
```{r}
lam <- 1/20  # set exponential parameters
pexp(5, rate=lam, lower.tail=TRUE)  # the person will survive <5 days
pexp(15, rate=lam, lower.tail=TRUE)-pexp(5, rate=lam, lower.tail=TRUE)  # 5-15 days
1-pexp(15, rate=lam, lower.tail=TRUE)  # at least 15 days
```

<br>
Generate 5000 random numbers from an Exp$(1/20)$. Compute the mean (i.e. $E(Y)$), the mean of the squared values (i.e., $E(Y^2)$) and the variance (i.e. var$(Y)$).
```{r}
set.seed(1)
yy <- rexp(n=1000, rate=lam)
EY <- mean(yy)  # should be close to 20
EY
EY2 <- mean(yy^2)  # should be close to 800
EY2
var(yy)  # should be close to 400
```



### Slide 67
1. Generate 1000 random values from a N(10,4). Create a Q-Q plot and add a line to check normality.
```{r}
yy <- rnorm(1000, mean=10, sd=sqrt(4))
qqnorm(yy, lwd=2, cex=1.3, cex.lab=1.3, cex.axis=1.3, cex.main=1.5)
qqline(yy, col=2, lwd=3)
```

<br>

2. Generate 1000 random variables from an Exp(1/20). Create a Q-Q plot and add a line to check normality.
```{r}
yy <- rexp(n=1000, rate=1/20)
qqnorm(yy, lwd=2, cex=1.3, cex.lab=1.3, cex.axis=1.3, cex.main=1.5)
qqline(yy, col=2, lwd=3)
```


3. Comment on the differences between the two plots.

### Slide 75

1. Create a matrix of size 100x500 of data generated from a normal distribution with some mean $\mu$ and some variance, $\sigma^2$.
```{r}
set.seed(2894)
YY <- matrix(rnorm(100*500, mean=-3, sd=2), nrow=100)  # 500 resamples from a N(-3,4)
YY[1:10,1:5]  # print the first 10 obs from the first 5 samples
```

<br>

2. Compute the mean of each sample (i.e., each column) and store this in a vector.
```{r}
Ybars <- colMeans(YY)  # compute the column means
length(Ybars)  # should be 500
Ybars[1:5]  # print the mean of the first 5 samples
```

<br>

3. Now, compute the mean and variance of the vector in (2).
```{r}
mean(Ybars) # should be close to -3, my specifed mu!
var(Ybars)  # should be close to 4/100!

qqnorm(Ybars, lwd=2, cex=1.3, cex.lab=1.3, cex.axis=1.3, cex.main=1.5)
qqline(Ybars, col=2, lwd=3)
```

<br>

4. Repeat (1) - (3) with data from a Bernoulli distribution.
```{r}
YY <- matrix(rbinom(100*500, size=1, prob=0.4), nrow=100)  # Bernoulli(0.4)
YY[1:10,1:5]  # print the first 10 obs from the first 5 samples

Ybars <- colMeans(YY)  # compute the column means
Ybars[1:5]  # print the mean of the first 5 samples

mean(Ybars) # should be close to 0.4, my specifed pi!
var(Ybars)  # should be close to 0.4*(1-0.4)/100 = 0.0024!

# by the CLT, this will also have a normal distribution!
qqnorm(Ybars, lwd=2, cex=1.3, cex.lab=1.3, cex.axis=1.3, cex.main=1.5)
qqline(Ybars, col=2, lwd=3)
```



### Slide 86
A researcher wants to investigate patient punctuality in keeping appointments. In a random sample of size $n=35$, he measures the number of minutes a patient is late (negative numbers mean arriving before the appointment). What is a $90\%$, $95\%$, and $99\%$ confidence interval for the population mean $\mu$?

```{r}
YY <- c(-33,  -1, -16, -21, -36,   6, -18,
         -3,   2, -25, -13, -16, -18, -31,
        -42,   8, -39, -11,   5,  10,  -9,
        -17, -17, -11, -34, -36,   4, -44,
        -13,   1, -15, -37, -40,  12, -20)
yb <- mean(YY);  s <- sd(YY);  n <- length(YY)  # sample statistics
yb  # sample mean
s  # sample standard deviation


#is the sample normal?
qqnorm(YY, lwd=2, cex=1.3, cex.lab=1.3, cex.axis=1.3, cex.main=1.5)
qqline(YY, col=2, lwd=3)

# 90 % CI
# t.test(x=YY, alternative="two.sided", mu=0, conf.level=0.90)
tcrit <- qt(0.95, df=n-1)
yb + c(-tcrit*s/sqrt(n), tcrit*s/sqrt(n))

# 95 % CI
# t.test(x=YY, alternative="two.sided", mu=0, conf.level=0.95)
tcrit <- qt(0.975, df=n-1)
yb + c(-tcrit*s/sqrt(n), tcrit*s/sqrt(n))

# 99 % CI
# t.test(x=YY, alternative="two.sided", mu=0, conf.level=0.99)
tcrit <- qt(0.995, df=n-1)
yb + c(-tcrit*s/sqrt(n), tcrit*s/sqrt(n))
```



<br>
<br>
<br>

## In-Depth Exercises
Below are exercises to reinforce concepts taught in the lecture part of the day.
<br>


### Question 1 - Thought question
Suppose an experiment involves randomly selecting one card from a standard 52 card deck.

Let $A=\{$A red card is pulled$\}$.

Let $B=\{$A face card is pulled$\}$.

a. What is $P(A)$?
b. What is $P(B)$?
c. What is $P(A \cap B)$?
d. What is $P(A \cup B)$?
e. Describe $\bar A$ and $\bar B$ in words.
f. What is the probability that the card pulled is a red, non-face card?
g. What is the probability that the card pulled is red or a non-face card?
h. Given that the card pull was red, what is the probability that it is a face card?

Solutions:

a. 1/2
b. 3/13
c. 3/26
d. 8/13
e. $\bar A$ is a black card is pulled. $\bar B$ is a non-face card is pulled.
f. 5/13
g. 23/26
h. 3/13


### Question 2
Suppose SBP among American adults without pre-diabetes or diabetes is normally distributed with mean $\mu=118$ mmHg and standard deviation $\sigma=15$ mmHg. Note that SBP $>=140$ mmHg is indicative of hypertension and $130<=$ SBP $<140$ is indicative of pre-hypertension. 

1. What is the probability that a person chosen at random is non-hypertensive?
```{r}
pnorm(q=130, mean=118, sd=15)
```

2. What is the probability that a person chosen at random is pre-hypertensive?
```{r}
pnorm(q=140, mean=118, sd=15)-pnorm(q=130, mean=118, sd=15)
```

3. What is the probability that a person chosen at random is hypertensive?
```{r}
# pnorm(q=140, mean=118, sd=15, lower.tail=FALSE)
1-pnorm(q=140, mean=118, sd=15)
```

4. What is the 10th percentile of SBP in this population?
```{r}
qnorm(0.1, mean=118, sd=15)
```



### Question 3
a. Read in the NHANES data. Use Q-Q plots to determine if age, SBP, ACR, eGFR, A1C, and PHQsum are approximately normally distributed.
```{r}
rm(list=ls())
NH <- read.csv(file="NHANES_hiv.csv", header=TRUE, na.strings=FALSE)

NHtmp <- subset(NH, select=c(age, SBP, ACR, eGFR, A1C, PHQsum))

par(mfrow=c(2,3))
for(ii in 1:ncol(NHtmp)){
    qqnorm(NHtmp[,ii], lwd=2, cex=1.3, cex.lab=1.3, cex.axis=1.3, cex.main=1.5,
        main=paste0("Q-Q Plot for ", names(NHtmp)[ii]))
      qqline(NHtmp[,ii], col=2, lwd=3)
}
```


b. (Thought question) For which of these variables do you think the sample mean is (approximately) normally distributed? (Consider your plots from last week as well.)
```{r}
par(mfrow=c(2,3))
for(ii in 1:ncol(NHtmp)){
    hist(NHtmp[,ii], cex=1.3, cex.lab=1.3, cex.axis=1.3, main="",
        xlab=names(NHtmp)[ii])
}
```

Solution: the sample mean for eGFR is definitely normal. For all others except ACR, the CLT says that the sample mean will be aproximately normal for sufficiently large n. This sample size is quite large so they are probably all ok, with the exception of ACR.


c. Compute the means for age, SBP, eGFR, A1C, and PHQsum.
```{r}
mean(NH$age, na.rm=TRUE)
mean(NH$SBP, na.rm=TRUE)
mean(NH$eGFR, na.rm=TRUE)
mean(NH$A1C, na.rm=TRUE)
mean(NH$PHQsum, na.rm=TRUE)
```



d. Take a simple random sample of size $n=100$ from the NHANES data. Use the sample to construct and interpret a $95\%$ CI for each of age, SBP, eGFR, A1C, and PHQsum. Supposing the values in (c) are the "true" population values, did your CIs capture them?
```{r}
NHsamp <- NH[sample(1:nrow(NH), 100, replace=FALSE),]  # dim(NHsamp)

#-----------------  Write a function that will compute CIs  ----------------#
CI <- function(x=NULL, a=0.05, mn, SD, n){
    if(!is.null(x)){  # if data entered,, use sample values for CI
        mn <- mean(x, na.rm=TRUE); SD <- sd(x, na.rm=TRUE); n <- length(x)
    }
    tcrit <- qt(c(a/2, 1-a/2), df=n-1)  # critical values for CI
    CI <- mn + tcrit*SD/sqrt(n)
    rslt <- c(CI[1], mn, CI[2])
    return(rslt)
}
#---------------------------------------------------------------------------#

CI(NHsamp$age)
CI(NHsamp$SBP)
CI(NHsamp$eGFR)
CI(NHsamp$A1C)
CI(NHsamp$PHQsum)
```


e. Use the sample to construct a $95\%$ CI for the population of proportion of individuals with a positive HSV-1 test.
```{r}
HSV1 <- na.omit(NHsamp$herpes1)
n <- length(HSV1)  # how many non-missing values? This is my sample size, not 100!
n

p <- mean(HSV1)  # sample proportion
p

zcrit <- qnorm(c(0.025, 0.975))  # critical values for CI
zcrit

p + zcrit*sqrt(p*(1-p))/sqrt(n)  # CI by Slutsky's and CLT

mean(NH$herpes1, na.rm=TRUE)  # compare to "population" proportion
```




